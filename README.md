**App Config example:**

```
#!xml
<configuration>
  <configSections>
    <section name="mailingService" type="MailingService.Configuration.MailingServiceSection, MailingService" />
  </configSections>
  <mailingService senderAddress="example@example.com" senderPassword="examplePass" senderDisplayName="example name" smtpProviderType="Office365"/>
</configuration>
```

**Available integrated SMTP providers:**

1) Google

2) Office365

**Usage:**


```
#!C#
EmailSenderService.GetSender().SendMailMessage(string subject, string body, params string[] receivers);

EmailSenderService.GetSender().SendMailMessageAsync(string subject, string body, params string[] receivers);

EmailSenderService.GetSender().SendMailMessageWithAttachments(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers);

EmailSenderService.GetSender().SendMailMessageWithAttachmentsAsync(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers);
```