﻿using System.Net;
using System.Net.Mail;

namespace MailingService.SmtpClientProviders
{
    public sealed class Office365SmtpClientProvider : ISmtpClientProvider
    {
        public SmtpClient CreateSmtpClient(string senderAddress, string senderPassword)
        {
            return new SmtpClient()
            {
                Port = 587,
                Host = "smtp.office365.com",
                EnableSsl = true,
                Timeout = 100000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderAddress, senderPassword),
                TargetName = "STARTTLS/smtp.office365.com"
            };
        }
    }
}
