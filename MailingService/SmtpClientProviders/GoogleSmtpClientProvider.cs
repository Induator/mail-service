﻿using System.Net;
using System.Net.Mail;

namespace MailingService.SmtpClientProviders
{
    public sealed class GoogleSmtpClientProvider : ISmtpClientProvider
    {
        public SmtpClient CreateSmtpClient(string senderAddress, string senderPassword)
        {
            return new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                Timeout = 100000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderAddress, senderPassword)
            };
        }
    }
}
