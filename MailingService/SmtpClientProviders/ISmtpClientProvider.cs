﻿using System.Net.Mail;

namespace MailingService.SmtpClientProviders
{
    public interface ISmtpClientProvider
    {
        SmtpClient CreateSmtpClient(string senderAddress, string senderPassword);
    }
}
