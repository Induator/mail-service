﻿using System;
using System.Collections.Generic;
using MailingService.Exceptions;

namespace MailingService.SmtpClientProviders
{
    public static class SmtpClientProviderTypeResolver
    {
        private static readonly Dictionary<SmtpProviderType, Type> EnumToTypeMap = new Dictionary<SmtpProviderType, Type>()
        {
            {SmtpProviderType.Google, typeof(GoogleSmtpClientProvider) },
            {SmtpProviderType.Office365, typeof(Office365SmtpClientProvider) }
        };

        public static Type Resolve(string type)
        {
            SmtpProviderType smtpProviderType;

            if (Enum.TryParse(type, true, out smtpProviderType))
            {
                return EnumToTypeMap[smtpProviderType];
            }

            throw new InvalidProviderTypeException();
        }

        public static Type Resolve(SmtpProviderType type)
        {
            return EnumToTypeMap[type];
        }
    }
}
