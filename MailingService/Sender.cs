﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using MailingService.SmtpClientProviders;

namespace MailingService
{
    public sealed class Sender : IDisposable
    {
        private readonly string _senderAddress;
        private readonly string _senderPassword;
        private readonly string _senderDisplayName;
        private readonly ISmtpClientProvider _smtpClientProvider;
        private readonly List<Task> _asyncSendingTasks = new List<Task>();

        public Sender(string senderAddress, string senderPassword, string senderDisplayName, ISmtpClientProvider smtpClientProvider)
        {
            _senderAddress = senderAddress;
            _senderPassword = senderPassword;
            _senderDisplayName = senderDisplayName;
            _smtpClientProvider = smtpClientProvider;
        }

        public void SendMailMessage(string subject, string body, params string[] receivers)
        {
            try
            {
                using (SmtpClient client = _smtpClientProvider.CreateSmtpClient(_senderAddress, _senderPassword))
                {
                    MailMessage mailMessage = CreateMailMessage(subject, body);
                    mailMessage.AddReceivers(receivers);
                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        public async void SendMailMessageAsync(string subject, string body, params string[] receivers)
        {
            try
            {
                using (SmtpClient client = _smtpClientProvider.CreateSmtpClient(_senderAddress, _senderPassword))
                {
                    MailMessage mailMessage = CreateMailMessage(subject, body);
                    mailMessage.AddReceivers(receivers);
                    Task sendMailTask = client.SendMailAsync(mailMessage);
                    _asyncSendingTasks.Add(sendMailTask);
                    await sendMailTask;
                    _asyncSendingTasks.Remove(sendMailTask);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public void SendMailMessageWithAttachments(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers)
        {
            try
            {
                using (SmtpClient client = _smtpClientProvider.CreateSmtpClient(_senderAddress, _senderPassword))
                using (MailMessage mailMessage = CreateMailMessage(subject, body))
                {
                    List<MemoryStream> memoryStreams = new List<MemoryStream>();
                    mailMessage.AddReceivers(receivers);

                    foreach (FileAttachment attachment in attachments)
                    {
                        MemoryStream stream = new MemoryStream(attachment.Data) { Position = 0 };
                        mailMessage.Attachments.Add(new Attachment(stream, attachment.FileName, attachment.MediaType));
                        memoryStreams.Add(stream);
                    }

                    client.Send(mailMessage);

                    foreach (MemoryStream memoryStream in memoryStreams)
                    {
                        memoryStream.Dispose();
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public async void SendMailMessageWithAttachmentsAsync(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers)
        {
            try
            {
                using (SmtpClient client = _smtpClientProvider.CreateSmtpClient(_senderAddress, _senderPassword))
                using (MailMessage mailMessage = CreateMailMessage(subject, body))
                {
                    List<MemoryStream> memoryStreams = new List<MemoryStream>();
                    mailMessage.AddReceivers(receivers);

                    foreach (FileAttachment attachment in attachments)
                    {
                        MemoryStream stream = new MemoryStream(attachment.Data) { Position = 0 };
                        mailMessage.Attachments.Add(new Attachment(stream, attachment.FileName, attachment.MediaType));
                        memoryStreams.Add(stream);
                    }

                    Task sendMailTask = client.SendMailAsync(mailMessage);
                    _asyncSendingTasks.Add(sendMailTask);
                    await sendMailTask;

                    foreach (MemoryStream memoryStream in memoryStreams)
                    {
                        memoryStream.Dispose();
                    }
                    _asyncSendingTasks.Remove(sendMailTask);

                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public void Dispose()
        {
            Task.WaitAll(_asyncSendingTasks.ToArray());
        }

        private MailMessage CreateMailMessage(string subject, string body)
        {
            return new MailMessage
            {
                BodyEncoding = Encoding.UTF8,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure,
                From = new MailAddress(_senderAddress, _senderDisplayName),
                Subject = subject,
                Body = body
            };
        }
    }
}
