﻿using System.Collections.Generic;
using MailingService.SmtpClientProviders;

namespace MailingService
{
    public class EmailSender
    {
        private readonly string _senderAddress;
        private readonly string _senderPassword;
        private readonly string _senderDisplayName;
        private readonly ISmtpClientProvider _smtpClientProvider;

        public EmailSender(string senderAddress, string senderPassword, string senderDisplayName, ISmtpClientProvider smtpClientProvider)
        {
            _senderAddress = senderAddress;
            _senderPassword = senderPassword;
            _senderDisplayName = senderDisplayName;
            _smtpClientProvider = smtpClientProvider;
        }

        public void SendMailMessage(string subject, string body, params string[] receivers)
        {
            using (Sender sender = new Sender(_senderAddress, _senderPassword, _senderDisplayName, _smtpClientProvider))
            {
                sender.SendMailMessage(subject, body, receivers);
            }
        }

        public void SendMailMessageAsync(string subject, string body, params string[] receivers)
        {
            using (Sender sender = new Sender(_senderAddress, _senderPassword, _senderDisplayName, _smtpClientProvider))
            {
                sender.SendMailMessageAsync(subject, body, receivers);
            }
        }

        public void SendMailMessageWithAttachments(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers)
        {
            using (Sender sender = new Sender(_senderAddress, _senderPassword, _senderDisplayName, _smtpClientProvider))
            {
                sender.SendMailMessageWithAttachments(subject, body, attachments, receivers);
            }
        }

        public void SendMailMessageWithAttachmentsAsync(string subject, string body, IEnumerable<FileAttachment> attachments, params string[] receivers)
        {
            using (Sender sender = new Sender(_senderAddress, _senderPassword, _senderDisplayName, _smtpClientProvider))
            {
                sender.SendMailMessageWithAttachmentsAsync(subject, body, attachments, receivers);
            }
        }

    }
}
