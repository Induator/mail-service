﻿namespace MailingService.Exceptions
{
    public sealed class ConfigurationFileNotFoundException : MailingServiceException
    {
    }
}
