﻿namespace MailingService.Exceptions
{
    public sealed class MailingServiceConfigurationSectionNotFoundException : MailingServiceException
    {
    }
}
