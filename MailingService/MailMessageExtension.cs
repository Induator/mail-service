﻿using System.Net.Mail;

namespace MailingService
{
    public static class MailMessageExtension
    {
        public static void AddReceivers(this MailMessage mailMessage, params string[] receivers)
        {
            foreach (string receiver in receivers)
            {
                mailMessage.To.Add(receiver);
            }
        }
    }
}
