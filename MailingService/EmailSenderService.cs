﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using MailingService.Configuration;
using MailingService.Exceptions;
using MailingService.SmtpClientProviders;

namespace MailingService
{
    public static class EmailSenderService
    {
        public static EmailSender GetSender(string senderAddress, string senderPassword, string senderDisplayName, ISmtpClientProvider provider)
        {
            return new EmailSender(senderAddress, senderPassword, senderDisplayName, provider);
        }

        public static EmailSender GetSender(string senderAddress, string senderPassword, string senderDisplayName, SmtpProviderType smtpProviderType)
        {
            ISmtpClientProvider smtpClientProvider = Activator.CreateInstance(SmtpClientProviderTypeResolver.Resolve(smtpProviderType)) as ISmtpClientProvider;
            return GetSender(senderAddress, senderPassword, senderDisplayName, smtpClientProvider);
        }
        /// <summary>
        /// Requires App.config
        /// </summary>
        /// <returns></returns>
        public static EmailSender GetSender()
        {
            MailingServiceSection section = GetMailingServiceSection();

            if(string.IsNullOrEmpty(section.SmtpProviderType))
                throw new SmtpProviderTypeConfigurationNotFoundException();

            ISmtpClientProvider smtpClientProvider = Activator.CreateInstance(SmtpClientProviderTypeResolver.Resolve(section.SmtpProviderType)) as ISmtpClientProvider;

            return GetSender(smtpClientProvider);
        }
        /// <summary>
        /// Requires App.config
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static EmailSender GetSender(ISmtpClientProvider provider)
        {
            MailingServiceSection section = GetMailingServiceSection();
            return GetSender(section.SenderAddress, section.SenderPassword, section.SenderDisplayName, provider);
        }

        public static bool IsValidMailAddress(string address)
        {
            try
            {
                new MailAddress(address);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static MailingServiceSection GetMailingServiceSection()
        {
            if (!File.Exists(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile))
                throw new ConfigurationFileNotFoundException();

            MailingServiceSection section = ConfigurationManager.GetSection(MailingServiceSection.SectionName) as MailingServiceSection;

            if (section == null)
                throw new MailingServiceConfigurationSectionNotFoundException();

            return section;
        }
    }
}
