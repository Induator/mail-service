﻿using System.Net.Mime;

namespace MailingService
{
    public sealed class FileAttachment
    {
        public string FileName { get; }
        public byte[] Data { get; }
        public string MediaType { get; }

        public FileAttachment(string fileName, byte[] data, string mediaType)
        {
            FileName = fileName;
            Data = data;
            MediaType = mediaType;
        }

        public FileAttachment(string fileName, byte[] data, ContentType contentType)
            : this(fileName, data, contentType?.MediaType)
        {
        }
    }
}
