﻿using System.Configuration;

namespace MailingService.Configuration
{
    public sealed class MailingServiceSection : ConfigurationSection
    {
        /// <summary>
        /// The name of this section in the app.config.
        /// </summary>
        public const string SectionName = "mailingService";

        [ConfigurationProperty("senderAddress", IsRequired = true)]
        public string SenderAddress
        {
            get { return (string)this["senderAddress"]; }
            set { this["senderAddress"] = value; }
        }

        [ConfigurationProperty("senderPassword", IsRequired = true)]
        public string SenderPassword
        {
            get { return (string)this["senderPassword"]; }
            set { this["senderPassword"] = value; }
        }

        [ConfigurationProperty("senderDisplayName", IsRequired = true)]
        public string SenderDisplayName
        {
            get { return (string)this["senderDisplayName"]; }
            set { this["senderDisplayName"] = value; }
        }

        [ConfigurationProperty("smtpProviderType", IsRequired = false)]
        public string SmtpProviderType
        {
            get { return (string)this["smtpProviderType"]; }
            set { this["smtpProviderType"] = value; }
        }
    }
}
